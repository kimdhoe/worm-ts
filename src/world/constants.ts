export const START = 'START'
export type START = typeof START

export const RESET = 'RESET'
export type RESET = typeof RESET

export const TICK = 'TICK'
export type TICK = typeof TICK

export const TURN = 'TURN'
export type TURN = typeof TURN

export const NEW_FOOD = 'NEW_FOOD'
export type NEW_FOOD = typeof NEW_FOOD

export const WIDTH = 20
export type WIDTH = typeof WIDTH

export const CELL_WIDTH = 25
export type CELL_WIDTH = typeof CELL_WIDTH
