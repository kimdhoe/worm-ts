const random = require('lodash.random')

import { StoreState, Cell, Worm, Direction } from '../type'
import { GameAction } from '../action'
import * as TYPE from '../constants'

const INITIAL_STATE: StoreState = { worm:      [ { x: 10, y: 10 } ]
                                  , food:      { x: random(0, 19)
                                               , y: random(0, 19)
                                               }
                                  , direction: ''
                                  , status:    'WAITING'
                                  }

function reducer (state: StoreState = INITIAL_STATE, action: GameAction): StoreState {
  switch (action.type) {
    case TYPE.START:
      if (state.status === 'WAITING') {
        return { ...state
               , status: 'PLAYING'
               }
      }

      return state

    case TYPE.RESET:
      return INITIAL_STATE

    case TYPE.TURN:
      return { ...state
             , direction: canTurn(state.worm, action.direction)
                            ? action.direction
                            : state.direction
             }

    case  TYPE.TICK:
      if (state.status !== 'PLAYING') {
        return state
      }

      const [ head ] = state.worm
      const aNewHead = newHead(head, state.direction)

      if (!canMove(state.worm, state.direction)) {
        return { ...state
               , status: 'GAME_OVER'
               }
      }

      const willEat = (aNewHead.x === state.food.x && aNewHead.y === state.food.y)
      const newWorm = [ aNewHead
                      , ...willEat ? state.worm
                                   : state.worm.slice(0, state.worm.length - 1)
                      ]
      const newFood = willEat ? { x: random(0, 19), y: random(0, 19) }
                              : state.food

      return { ...state
             , worm: newWorm
             , food: newFood
             }

    default:
      return state
  }
}

function newHead (head: Cell, direction: Direction): Cell {
  return direction === 'UP'    ? { x: head.x,     y: head.y - 1 } :
         direction === 'RIGHT' ? { x: head.x + 1, y: head.y     } :
         direction === 'DOWN'  ? { x: head.x,     y: head.y + 1 } :
         direction === 'LEFT'  ? { x: head.x - 1, y: head.y     } :
         /* else */              head
}

function canTurn (worm: Worm, direction: Direction): boolean {
  if (worm.length === 1) {
    return true
  }

  const aNewHead = newHead(worm[0], direction)

  return aNewHead.x !== worm[1].x || aNewHead.y !== worm[1].y
}

function canMove (worm: Worm, direction: Direction): boolean {
  const [ head, ...tail ] = worm
  const aNewHead = newHead(head, direction)

  return head.x >= 0 && head.y >= 0 && head.x <= 19 && head.y <= 19
      && tail.every(cell =>
           aNewHead.x !== cell.x || aNewHead.y !== cell.y
         )

}

export default reducer
