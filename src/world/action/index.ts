import { Direction } from '../type'
import * as constants from '../constants'

type GameAction = Tick
                | Turn
                | Start
                | Reset
                | NewFood

interface Tick {
  type: constants.TICK
}

interface Turn {
  type:      constants.TURN
  direction: Direction
}

interface Start {
  type: constants.START
}

interface Reset {
  type: constants.RESET
}

interface NewFood {
  type: constants.NEW_FOOD
}

const tick = (): Tick => (
  { type: constants.TICK }
)

const turn = (direction: Direction): Turn => (
  { type: constants.TURN
  , direction
  }
)

const start = (): Start => (
  { type: constants.START }
)

const reset = (): Reset => (
  { type: constants.RESET }
)

export { GameAction
       , Tick
       , Start
       , Reset
       , tick
       , turn
       , start
       , reset
       }
