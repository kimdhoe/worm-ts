interface StoreState {
  worm: Worm
  food: Cell
  direction: Direction
  status: GameStatus
}

type Worm = Array<Cell>

interface Cell {
  x: number
  y: number
}

type Direction = 'UP' | 'RIGHT' | 'DOWN' | 'LEFT' | ''

type GameStatus = 'WAITING' | 'PLAYING' | 'GAME_OVER'

export { StoreState
       , Worm
       , Cell
       , Direction
       , GameStatus
       }
