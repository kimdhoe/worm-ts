import * as React from 'react'

import './App.css'
import { Worm, Cell, GameStatus } from '../world/type'
import { SceneComponent } from './Scene'
import { Start, Reset } from '../world/action'

interface Props {
  worm: Worm
  food: Cell
  status: GameStatus
  installTick: () => void
  installTurnHandler: () => void
  start: () => Start
  reset: () => Reset
}

class AppComponent extends React.Component<Props, object> {
  private isTickInstalled: boolean

  constructor() {
    super()

    this.isTickInstalled = false
  }

  componentDidMount() {
    this.props.installTurnHandler()

    window.addEventListener('keydown', this.initialKeyHandler)
  }

  initialKeyHandler = () => {
    this.props.start()

    if (!this.isTickInstalled) {
      this.props.installTick()
      this.isTickInstalled = true
    }

    {
      /* window.removeEventListener('keydown', this.initialKeyHandler) */
    }
  }

  playAgain = () => {
    this.props.reset()
  }

  render() {
    const { worm, food, status, reset } = this.props

    return (
      <div className="App">
        <div className="App-header">
          <h1>Worm</h1>
          <h2>{worm.length}</h2>
        </div>

        <div>
          <SceneComponent
            worm={worm}
            food={food}
            status={status}
            reset={reset}
          />
        </div>
      </div>
    )
  }
}

export default AppComponent
export { Props }
