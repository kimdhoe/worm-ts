import * as React from 'react'

import { Worm, Cell, GameStatus } from '../../world/type'
import { WormComponent, CellComponent }  from '../Worm'
import { Reset } from '../../world/action'
import './Scene.css'

interface Props {
  worm: Worm
  food: Cell
  status: GameStatus
  reset: () => Reset
}

const SceneComponent = ({ worm, food, status, reset }: Props) => (
  <div className="Scene">
    <WormComponent worm={worm} />

    <div
      className="Scene-food"
      style={{ left: `${25 * food.x}px`
      , top:  `${25 * food.y}px`
      }}
    >
      <CellComponent color="red" />
    </div>

    {status === 'WAITING' && (
      <div className="Scene-message">
        <p>Press arrow key to play.</p>
      </div>
    )}

    {status === 'GAME_OVER' && (
      <div className="Scene-message">
        <p>
          GAME OVER
        </p>
        <button onClick={reset}>Play Again</button>
      </div>
    )}
  </div>
)

export default SceneComponent
