import { connect, Dispatch } from 'react-redux'

import { StoreState } from '../world/type'
import * as action from '../world/action'
import AppComponent from './App.component'

const mapStateToProps = ({ worm, food, status }: StoreState) => ({
  worm,
  food,
  status
})

const mapDispatchToProps = (dispatch: Dispatch<action.GameAction>) => ({
  installTurnHandler: () => {
    window.addEventListener('keydown', e => {
      if (e.key === 'ArrowUp') {
        dispatch(action.turn('UP'))
      } else if (e.key === 'ArrowRight') {
        dispatch(action.turn('RIGHT'))
      } else if (e.key === 'ArrowDown') {
        dispatch(action.turn('DOWN'))
      } else if (e.key === 'ArrowLeft') {
        dispatch(action.turn('LEFT'))
      }
    })
  },

  installTick: () => {
    dispatch(action.start())
    window.setInterval(() => dispatch(action.tick()), 200)
  },
  start: () => dispatch(action.start()),
  reset: () => dispatch(action.reset())
})

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(AppComponent)

export default AppContainer
