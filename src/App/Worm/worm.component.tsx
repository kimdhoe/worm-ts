import * as React from 'react'

import { Worm } from '../../world/type'
import { CellComponent } from '.'
import './worm.css'

interface Props {
  worm: Worm
}

const WormComponent = ({ worm }: Props) => (
  <div className="Worm">
    {worm.map((cell, i) => (
      <div
        key={i}
        className="Worm-cell"
        style={
          { left: 25 * cell.x
          , top:  25 * cell.y
          }
        }
      >
        <CellComponent />
      </div>
    ))}
  </div>
)

export default WormComponent
