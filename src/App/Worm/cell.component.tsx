import * as React from 'react'

import './cell.css'

interface Props {
  color?: string
}

const CellComponent = ({ color }: Props) => (
  <div
    className="Cell"
    style={color ? { backgroundColor: color } : {}}
  />
)

export default CellComponent
