import WormComponent from './worm.component'
import CellComponent from './cell.component'

export { CellComponent
       , WormComponent
       }
